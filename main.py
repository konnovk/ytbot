if __name__ == '__main__':
    from loguru import logger
    from bot.settings import settings
    from bot.bot import bot

    logger.add('.log/.log', rotation="1 week")

    logger.info(f'START TELEGRAM BOT')
    if settings.debug:
        logger.warning(f'RUNNING IN DEBUG MODE')
    try:
        bot.polling()
    except Exception as e:
        logger.exception(e)
