from telebot import TeleBot
from telebot.types import Message

from bot.handlers.base_handler import BaseHandler
from bot.utils import debug_message


class HelloHandler(BaseHandler):
    def __init__(self, bot: TeleBot):
        super().__init__(bot)
        with open('bot/messages/hello.txt', 'r', encoding='utf-8') as f:
            self.hello_text = f.read()

    def action(self, message: Message):
        self.send_message(message, self.hello_text)
        debug_message(self._bot, message, "Сейчас я запущен в отладочном режиме")
