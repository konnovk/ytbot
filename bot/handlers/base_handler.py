import secrets

from loguru import logger
from telebot import TeleBot
from telebot.types import Message


class BaseHandler:
    def __init__(self, bot: TeleBot):
        self._handler_id = secrets.token_hex(8)
        logger.info(f"CREATE HANDLER WITH ID ${self._handler_id}")
        self._bot = bot

    def send_message(self, message: Message, text: str):
        logger.info(f"{self._handler_id} SEND MESSAGE TO [{message.chat.id} {message.chat.username}]:\n{text}")
        self._bot.send_message(message.chat.id, text)

    def handle(self, message: Message):
        logger.info(
            f"{self._handler_id} GET MESSAGE FROM [{message.chat.id} {message.chat.username}]:\n"
            f"{message.text}"
        )
        self.before_action(message)
        self.action(message)
        self.after_action(message)

    def before_action(self, message: Message):
        pass

    def after_action(self, message: Message):
        pass

    def action(self, message: Message):
        self.send_message(
            message,
            "Сообщение отправлено из базового обработчика. Если вы видите его, значит что-то пошло не так"
        )
