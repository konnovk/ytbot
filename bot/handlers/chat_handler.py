import collections

import pytube
from pytube import Search, YouTube
from pytube.exceptions import RegexMatchError
from telebot import TeleBot
from telebot.apihelper import ApiTelegramException
from telebot.types import Message, InputFile

from bot.handlers.base_handler import BaseHandler
from bot.media_processor import MediaProcessor


class ChatHandler(BaseHandler):
    def __init__(self, bot: TeleBot):
        super().__init__(bot)
        self.chats = collections.defaultdict(collections.deque)
        self.search_results = collections.defaultdict(list)
        self.media_processor = MediaProcessor()

    def before_action(self, message: Message):
        chat_id = message.chat.id
        self.chats[chat_id].append(message)

    def action(self, message: Message):
        chat_id = message.chat.id
        in_text = message.text
        if in_text == '/cancel':
            self.send_message(message, f'Отмена действия. Теперь ты можешь начать сначала.')
            return
        if len(self.chats[chat_id]) == 1:
            self._process_with_url(message)
        elif len(self.chats[chat_id]) >= 2:
            self._process_with_answer(message)

    def after_action(self, message: Message):
        chat_id = message.chat.id
        in_text = message.text
        if in_text == '/cancel':
            self.chats[chat_id].clear()

    def _process_with_url(self, message: Message):
        chat_id = message.chat.id
        url = message.text
        self.send_message(message, f"Сейчас обработаю...")
        try:
            mp4_file = self.media_processor.download_video_by_url(url)
            self._proccess_video_by_url(message, chat_id, mp4_file)
        except RegexMatchError:
            self._process_with_query(message)
            return

    def _process_with_query(self, message: Message):
        in_text = message.text
        chat_id = message.chat.id
        videos: list[YouTube] = Search(in_text).results
        text = "Я нашел несколько видео по названию, какое из них использовать?\n"
        for i, v in enumerate(videos[:10]):
            text += f'✅    {i+1}    🚹    {v.author}    ⏩    {v.title}\n'
        self.send_message(message, text)
        self.search_results[chat_id] = videos[:10]

    def _process_with_answer(self, message: Message):
        chat_id = message.chat.id
        in_text = message.text
        try:
            ans = int(in_text)
            if ans not in range(1, 11):
                raise ValueError
        except ValueError:
            self.send_message(message, f"Такого варианта не было, попробуй еще раз...")
            return
        video: pytube.YouTube = self.search_results[chat_id][ans-1]
        self.send_message(message, f"Сейчас обработаю выбранное видео...")
        self.send_message(message, video.embed_url)
        url = video.embed_url
        mp4_file = self.media_processor.download_video_by_url(url)
        self._proccess_video_by_url(message, chat_id, mp4_file)

    def _proccess_video_by_url(self, message, chat_id, mp4_file):
        mp3_file = f'{mp4_file}.mp3'
        self.media_processor.convert_to_mp3(mp4_file, mp3_file)
        self.media_processor.delete_file(mp4_file)
        try:
            self.send_message(message, "Ваш аудиофайл готов, вот он:")
            self._bot.send_audio(
                message.chat.id,
                InputFile(mp3_file)
            )
            self.send_message(message, "Спасибо, что использовали меня, давайте еще раз!!! ❤❤❤❤❤")
            self.chats[chat_id].clear()
        except ApiTelegramException as exc:
            if exc.error_code == 413:
                self.send_message(
                    message,
                    "Упс, ваше видео оказалось слишком большим, не могу его обработать 😭😭😭😭😭"
                )
        self.media_processor.delete_file(mp3_file)
        self.chats[chat_id].clear()
        self.search_results[chat_id].clear()
