from loguru import logger
from pydantic import ValidationError
from pydantic_settings import BaseSettings, SettingsConfigDict


class Settings(BaseSettings):
    model_config = SettingsConfigDict(env_file='.env', env_file_encoding='utf-8')
    token: str
    debug: bool = False


try:
    settings = Settings()
except ValidationError as err:
    logger.exception(err)
