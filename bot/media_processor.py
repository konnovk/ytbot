import os
import secrets

import pytube
from loguru import logger
from moviepy.video.io.VideoFileClip import VideoFileClip


class MediaProcessor:

    def __init__(self):
        pass

    def download_video_by_url(self, url: str) -> str:
        '''
        url - url видоса с ютуба, который надо скачать
        '''
        tmp_token = secrets.token_hex(8)
        media_folder = f'media/{tmp_token}'

        video = pytube.YouTube(url)

        output_path = os.path.join(os.getcwd(), media_folder)
        if not os.path.exists(output_path):
            logger.debug(f'Создание папки {media_folder}...')
            os.makedirs(output_path)

        stream = video.streams.get_highest_resolution()

        logger.debug('Загрузка видео...')
        output_file = stream.download(output_path)

        logger.debug(f'Видео успешно загружено: {output_file}')
        return output_file

    def convert_to_mp3(self, mp4: str, mp3: str):
        '''
        mp4 - путь к файлу с видео
        mp3 - путь к целевому файлу со звуком
        '''
        logger.debug(f'Создание аудио из видео: {mp3}')
        video = VideoFileClip(mp4)
        audio = video.audio
        audio.write_audiofile(mp3)
        video.close()
        audio.close()

    def delete_file(self, path: str):
        logger.debug(f'Удаление файла: {path}')
        os.remove(path)
