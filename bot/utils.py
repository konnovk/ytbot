import telebot
from telebot.types import Message
from loguru import logger

from bot.settings import settings


def reply_not_implemented(bot: telebot.TeleBot, message: Message):
    bot.reply_to(message, "Данная функция пока что находится на стадии разработки!\n😭😭😭😭😭")


def debug_message(bot: telebot.TeleBot, message: Message, text: str):
    if settings.debug:
        bot.send_message(message.chat.id, f"⚡⚡⚡⚡⚡ | СООБЩЕНИЕ ОТЛАДКИ | {message.chat.id}\n{text}")


def debug_logger_wrapper(func):
    def wrapper(message):
        if settings.debug:
            handler_id = logger.add(lambda record: debug_message(bot, message, record), format="{message}")
            res = func(message)
            logger.remove(handler_id)
        else:
            res = func(message)
        return res

    return wrapper
