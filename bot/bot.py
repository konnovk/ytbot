import telebot
from telebot.types import Message

from bot.handlers.chat_handler import ChatHandler
from bot.settings import settings
from bot.utils import debug_logger_wrapper

from bot.handlers.base_handler import BaseHandler
from bot.handlers.hello_handler import HelloHandler

telebot.apihelper.ENABLE_MIDDLEWARE = True
bot = telebot.TeleBot(settings.token)

base_handler = BaseHandler(bot)
hello_handler = HelloHandler(bot)
chat_handler = ChatHandler(bot)


def setup_bot(bot: telebot.TeleBot):
    @bot.message_handler(commands=['help', 'start'])
    def hello_message(message: Message):
        hello_handler.handle(message)

    @bot.message_handler(content_types=['text'])
    @debug_logger_wrapper
    def convert_url(message: Message):
        chat_handler.handle(message)


setup_bot(bot)
